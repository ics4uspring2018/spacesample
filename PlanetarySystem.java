
/**
 * PlanetarySystem.
 * 
 * @author Mr. Chan 
 * @version September 2015
 */

import java.util.Random;

public class PlanetarySystem
{
    String name;  // A property of PlanetarySystem
    Planet[] planets;
    PlanetarySystem(String name, Planet[] planets) {  // Constructor
        this.name = name;
        this.planets = planets;
    }
    
    public Planet randomPlanet() {
        Random rng = new Random();
        if (!(planets == null || planets.length == 0)) {
            int index = rng.nextInt(planets.length);
            return planets[index];
        }
        return null;
    }
}
