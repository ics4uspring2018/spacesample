
/**
 * Planet.
 * 
 * @author Mr. Chan 
 * @version September 2015
 */
public class Planet
{
    String name;
    String description;
    
    // Constructor
    public Planet(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
