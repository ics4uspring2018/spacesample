
/**
 * A SpaceAdventure: Review of Java Programming.
 * 
 * @author Mr. Chan 
 * @version September 2015
 */

import java.util.Scanner;

public class SpaceAdventure
{
    PlanetarySystem planetarySystem; 

    public SpaceAdventure() {
        Planet[] planets = new Planet[8];
        Planet mercury = new Planet("Mercury", "A very hot planet, closest to the sun.");
        Planet venus = new Planet("Venus", "It's very cloudy here!");
        Planet earth = new Planet("Earth", "There is something very familiar about this planet.");
        Planet mars = new Planet("Mars", "Known as the red planet.");
        Planet jupiter = new Planet("Jupiter", "A gas giant, with a noticeable red spot.");
        Planet saturn = new Planet("Saturn", "This planet has beautiful rings around it.");
        Planet uranus = new Planet("Uranus", "Strangely, this planet rotates around on its side.");
        Planet neptune = new Planet("Neptune", "A very cold planet, furthest from the sun.");
        planets[0] = mercury;
        planets[1] = venus;
        planets[2] = earth;
        planets[3] = mars;
        planets[4] = jupiter;
        planets[5] = saturn;
        planets[6] = uranus;
        planets[7] = neptune;
        planetarySystem = new PlanetarySystem("Solar System", planets);

    }

    private void displayIntroduction() {
        final int numberOfPlanets = 8;
        final double circumferenceOfEarth = 24859.82;  // in miles, from pole to pole.
        println("Welcome to our solar system!");
        println("There are " + numberOfPlanets + " planets to explore.");
        println("You are currently on Earth, which has a circumference of " +
            circumferenceOfEarth + " miles.");
    }

    private String responseToPrompt(String prompt) {
        print(prompt);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    private void greetAdventurer() {
        String name = responseToPrompt("What is your name?  ");
        System.out.println("\nNice to meet you, " + name + ". My name is Eliza.");
    }

    private void determineDestination() {
        String decision = "";
        while (!(decision.equals("Y") || decision.equals("N"))) {
            decision = responseToPrompt("Shall I randomly choose a planet for you to visit? (Y or N)  ");
            if (decision.equals("Y") ) {
                Planet planet = planetarySystem.randomPlanet();
                if (planet != null) {
                    visit(planet.name);
                } else {
                    println("Sorry, but there are no planets in this system.");
                }
            } else if (decision.equals("N") ) {
                String planetName = responseToPrompt("Ok, name the planet you would like to visit:");
                visit(planetName);
            } else {
                println("Huh? Sorry, I didn't get that.");
            }
        }
    }

    private void visit(String planetName) {
        println("Travelling to " + planetName + " ...");
        for (Planet planet: planetarySystem.planets) {
            if (planetName.equals(planet.name)) {
                println("Arrived at " + planet.name + ".  " + planet.description);
            }
        }
    }

    private void start() {
        displayIntroduction();
        greetAdventurer();
        if (!(planetarySystem.planets == null || planetarySystem.planets.length == 0)) {
            println("Let's go on an adventure!");
            determineDestination();
        }
    }

    public static void main() {
        SpaceAdventure adventure = new SpaceAdventure();
        adventure.start();
    }

    /**
     * Helper methods
     */
    private static void print(String phrase){
        System.out.print(phrase);
    }

    private static void println(String phrase){
        System.out.println(phrase);
    }

}
